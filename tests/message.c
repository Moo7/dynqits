#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dynqits_msg.h"

int main(int argc, char *argv[])
{
	const char *s = "[\"Hello\", \"MessagePack\"]";
	dynqits_buf_t buf;
	dynqits_msg_t *msg;

	uuid_t uu;
	uuid_generate(uu);

	buf =
		dynqits_encode(uu, 1234, DYNQ_ENC_JSON, DYNQ_MSG_ADVERT, s,
					   strlen(s) + 1);

	msg = dynqits_decode(buf.p, buf.size);
	if (msg == NULL) {
		fprintf(stderr, "decode failed\n");
		exit(EXIT_FAILURE);
	}

	char u[37];
	uuid_unparse(msg->uuid, u);
	printf("uuid     = %s\n", u);
	printf("station  = %lu\n", (long unsigned int) msg->unit);
	printf("version  = %u\n", msg->version);
	printf("encoding = %u\n", msg->encoding);
	printf("type     = %u\n", msg->type);
	printf("msg      = %s\n", (char *) msg->buf.p);
	printf("len      = %lu\n", (long unsigned int) msg->buf.size);

	dynqits_msg_delete(msg);
	return EXIT_SUCCESS;
}
