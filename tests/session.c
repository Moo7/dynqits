#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>

#include "dynqits.h"

#define BROKER     "tcp://localhost:1883"
#define UNIT		1234
#define PAYLOAD     "Hello World!"
#define QOS         1
#define TIMEOUT     10000L

volatile int deliveredtoken = 0;
volatile bool arrived=false;

static void msg_delivered(void *user, int dt)
{
	printf("message with token value %d delivery confirmed\n", dt);
	deliveredtoken = dt;
}

static int
msg_incoming(void *user, dynqits_typ_t type, void *msg, size_t size)
{
	char *s = (char *) msg;

	printf("message arrived\n");
	printf("      type: %i\n", type);
	printf("   content: ");

	for (int i = 0; i < size; i++) {
		putchar(*s++);
	}
	putchar('\n');
	arrived=true;
	return 1;
}

static void msg_exception(void *user, const char *cause, dynqits_exc_t exc)
{
	printf("\exception\n");
	printf("      code: %i\n", exc);
	printf("     cause: %s\n", cause);
}

int main(int argc, char *argv[])
{
	int rc, token;
	void *handle = NULL;

	dynqits_init_t init = {
		BROKER,
		UNIT,
		NULL,
		DYNQ_ENC_JSON,
		DYNQ_QOS_ATMOST_ONCE,
		msg_delivered,
		msg_incoming,
		msg_exception
	};

	uuid_t uuid;
	uuid_generate(uuid);

	rc = dynqits_create(&init, uuid, &handle);
	if (rc < 0) {
		fprintf(stderr, "dynqits: failed to create session");
		exit(EXIT_FAILURE);
	}

	rc = dynqits_connect(handle);
	if (rc < 0) {
		fprintf(stderr, "dynqits: failed to connect");
		exit(EXIT_FAILURE);
	}
	
	// REMARK: as messages are not buffered, need to subscribe before publish
	// subscribe to all topics
	rc = dynqits_subscribe(handle, DYNQ_QOS_DEFAULT, NULL, NULL);
	if (rc < 0) {
		fprintf(stderr, "dynqits: subscription failed");
		exit(EXIT_FAILURE);
	}

	rc = dynqits_publish(handle, DYNQ_MSG_TEST, PAYLOAD,
						 strlen(PAYLOAD) + 1, DYNQ_QOS_DEFAULT, &token);
	if (rc < 0) {
		fprintf(stderr, "dynqits: failed to publish message");
		exit(EXIT_FAILURE);
	}
			
	printf("waiting for publication ...\n");

	while (deliveredtoken != token && arrived!=true);
	
	sleep(1); // allow stdout to be written
	printf("\nbye.\n");

	dynqits_destroy(handle);
	return rc;
}
