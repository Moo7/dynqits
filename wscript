#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

import os
import waftools

top='.'
out='build'


APPNAME='dynqits'
VERSION='0.0.1'


def options(opt):
	opt.load('ccenv', tooldir=waftools.location)


def configure(conf):
	conf.load('ccenv')
	conf.env.append_unique('INCLUDES', conf.path.find_node(out).abspath())
	conf.check(header_name='stdlib.h', features='c cprogram')
	conf.check(header_name='stdio.h', features='c cprogram')
	conf.check(header_name='string.h', features='c cprogram')
	conf.check(header_name='MQTTClient.h', features='c cprogram')
	conf.write_config_header('config.h')
	conf.env.VNUM = VERSION


def build(bld):
	## TODO: test using local libs
	bld.read_shlib('paho-mqtt3c', paths=["%s/lib" % bld.env.PREFIX])		
	bld.read_shlib('uuid')
	bld.read_shlib('msgpack')

	bld.recurse('lib')
	bld.recurse('tests')

