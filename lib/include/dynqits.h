
#include <stddef.h>
#include <stdint.h>
#include <uuid/uuid.h>

#ifndef DYNQITS_H_
#define DYNQITS_H_

#define DYNQITS_VERSION 1

#define DYNQ_MSG_NAME_TEST		"test"
#define DYNQ_MSG_NAME_ADVERT	"advert"
#define DYNQ_MSG_NAME_COMMAND	"cmd"
#define DYNQ_MSG_NAME_STATUS	"sts"

typedef enum dynqits_typ {
	DYNQ_MSG_TEST = 0,
	DYNQ_MSG_ADVERT = 1,
	DYNQ_MSG_COMMAND = 2,
	DYNQ_MSG_STATUS = 3
} dynqits_typ_t;
#define DYNQ_TYP_FIRST DYNQ_MSG_TEST
#define DYNQ_TYP_LAST DYNQ_MSG_STATUS

typedef enum dynqits_enc {
	DYNQ_ENC_UPER = 1,			// ANS.1 Packet Encoding Rules (UPER)
	DYNQ_ENC_XML = 2,
	DYNQ_ENC_JSON = 3,
	DYNQ_ENC_MSGPACK = 4
} dynqits_enc_t;

typedef enum dynqits_qos {
	DYNQ_QOS_DEFAULT = 0,		// use connection default       
	DYNQ_QOS_ATMOST_ONCE = 1,
	DYNQ_QOS_ATLEAST_ONCE = 2,
	DYNQ_QOS_EXACTLY_ONCE = 3
} dynqits_qos_t;
#define DYNQ_QOS_FIRST DYNQ_QOS_DEFAULT
#define DYNQ_QOS_LAST DYNQ_QOS_EXACTLY_ONCE

typedef enum dynqits_exc {
	DYNQ_EXC_CONNLOST = 1,

} dynqits_exc_t;

typedef void (*dynqits_delivered_f) (void *user, int token);
typedef int (*dynqits_incoming_f) (void *user, dynqits_typ_t type,
								   void *msg, size_t size);
typedef void (*dynqits_exception_f) (void *user, const char *cause,
									 dynqits_exc_t exc);

typedef struct dynqits_init {
	/** broker hostname and socket (e.g. "tcp://localhost:1883") */
	const char *broker;

	/** unit number (configurable) */
	uint32_t unit;

	/** user context, will be available in callbacks */
	void *user;

	/** message encoding to be used for all publications */
	dynqits_enc_t encoding;

	/** default quality of service to be used, may be overriden per 
	 * publication.
	 */
	dynqits_qos_t qos;

	/** callback to be invoked when message has arrived at broker */
	dynqits_delivered_f delivered;

	/** callback to be invoked when new messages arrive */
	dynqits_incoming_f incoming;

	/** exception and error handler */
	dynqits_exception_f exception;

} dynqits_init_t;

int dynqits_create(dynqits_init_t * init, uuid_t uuid, void **handle);
int dynqits_destroy(void *handle);

int dynqits_connect(void *handle);
int dynqits_disconnect(void *handle);

int dynqits_publish(void *handle, dynqits_typ_t type, const void *buf,
					size_t size, dynqits_qos_t qos, int *token);

int dynqits_subscribe(void *handle, dynqits_qos_t qos, dynqits_typ_t * type, 
					  uint32_t * unit);

#endif							// DYNQITS_H_
