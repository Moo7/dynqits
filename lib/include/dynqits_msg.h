/**
	Encodes and decodes messages using msgpack.

	Messages are piggy-backed on containers providing meta information
	on the actual message being transferred.

	Message are encoded as array in message containing following
	elements:
	
	[
		bin[16],	// uuid; 16 byte array binary encoded
		uint32		// unit number
		uint16		// container version
		uint16		// encoding type
		uint16		// message type
		bin[x]		// message and length as provided by user
	]

*/

#include <stddef.h>
#include <stdint.h>
#include <uuid/uuid.h>

#include "dynqits.h"

#ifndef DYNQITS_MSG_H_
#define DYNQITS_MSG_H_

#define DYNQITS_MSG_VERSION 1
#define DYNQITS_MSG_SIZE 6

typedef struct dynqits_buf {
	void *p;
	size_t size;
} dynqits_buf_t;

typedef struct dynqits_msg {

	/** unique unit identifier */
	uuid_t uuid;

	/** unit number (configurable) */
	uint32_t unit;

	/** container version */
	uint16_t version;

	/** encoding of received message */
	dynqits_enc_t encoding;

	/** recieved message type */
	dynqits_typ_t type;

	/** received message */
	dynqits_buf_t buf;

} dynqits_msg_t;

void dynqits_buf_delete(dynqits_buf_t buf);

void dynqits_msg_delete(dynqits_msg_t * msg);

dynqits_buf_t dynqits_encode(uuid_t uu, uint32_t unit,
							 dynqits_enc_t encoding, dynqits_typ_t type,
							 const void *buf, const size_t size);

dynqits_msg_t *dynqits_decode(const void *buf, const size_t size);

#endif							// DYNQITS_MSG_H_
