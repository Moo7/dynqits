#include <stdio.h>
#include <assert.h>
#include <msgpack.h>

#include "dynqits_msg.h"

void dynqits_buf_delete(dynqits_buf_t buf)
{
	if (buf.p != NULL)
		free(buf.p);
}

void dynqits_msg_delete(dynqits_msg_t * msg)
{
	if (msg == NULL)
		return;

	dynqits_buf_delete(msg->buf);
	free(msg);
}

dynqits_buf_t
dynqits_encode(uuid_t uu, uint32_t unit,
			   dynqits_enc_t encoding, dynqits_typ_t type,
			   const void *buf, const size_t size)
{
	assert(uu != NULL);
	assert(buf != NULL);
	assert(size > 0);

	msgpack_sbuffer sbuf;
	msgpack_sbuffer_init(&sbuf);
	msgpack_packer pk;
	msgpack_packer_init(&pk, &sbuf, msgpack_sbuffer_write);

	// container structure encoded as array
	msgpack_pack_array(&pk, DYNQITS_MSG_SIZE);

	// (1) uuid
	msgpack_pack_bin(&pk, sizeof(uuid_t));
	msgpack_pack_bin_body(&pk, uu, sizeof(uuid_t));

	// (2) station
	msgpack_pack_uint32(&pk, unit);

	// (3) container version
	msgpack_pack_uint16(&pk, DYNQITS_MSG_VERSION);

	// (4) payload (message) encoding
	msgpack_pack_uint16(&pk, encoding);

	// (5) payload (message) type
	msgpack_pack_uint16(&pk, type);

	// (6) payload
	msgpack_pack_bin(&pk, size);
	msgpack_pack_bin_body(&pk, buf, size);

	// store results
	dynqits_buf_t dbuf;
	dbuf.size = sbuf.size;
	dbuf.p = malloc(dbuf.size);
	memcpy(dbuf.p, sbuf.data, dbuf.size);
	return dbuf;
}

dynqits_msg_t *dynqits_decode(const void *buf, const size_t size)
{
	dynqits_msg_t *msg = NULL;
	assert(buf != NULL);
	assert(size > 0);

	msgpack_zone mempool;
	msgpack_zone_init(&mempool, 2048);
	msgpack_object obj;
	msgpack_unpack(buf, size, NULL, &mempool, &obj);

	if (obj.type != MSGPACK_OBJECT_ARRAY) {
		fprintf(stderr, "%s(%i): decode failed, illegal type(%i)\n",
				__FILE__, __LINE__, obj.type);
		goto decode_failed;
	}

	msgpack_object_array array = obj.via.array;

	if (array.size < DYNQITS_MSG_SIZE) {
		fprintf(stderr, "%s(%i): decode failed, array too small(%i)\n",
				__FILE__, __LINE__, array.size);
		goto decode_failed;
	}

	struct msgpack_object *p = array.ptr;

	// (1) unique unit identifier
	if (p->type != MSGPACK_OBJECT_BIN) {
		fprintf(stderr, "%s(%i): decode failed, illegal type(%i)\n",
				__FILE__, __LINE__, p->type);
		goto decode_failed;
	}
	if (p->via.bin.size != sizeof(uuid_t)) {
		fprintf(stderr, "%s(%i): decode failed, illegal uuid size(%i)\n",
				__FILE__, __LINE__, p->via.bin.size);
		goto decode_failed;
	}
	msg = calloc(1, sizeof(*msg));
	memcpy(&(msg->uuid), p->via.bin.ptr, sizeof(uuid_t));
	p++;

	// (2) unit number (configurable)
	if (p->type != MSGPACK_OBJECT_POSITIVE_INTEGER) {
		fprintf(stderr, "%s(%i): decode failed, illegal type(%i)\n",
				__FILE__, __LINE__, p->type);
		goto decode_failed;
	}
	msg->unit = (uint32_t) p->via.u64;
	p++;

	// (3) container version
	if (p->type != MSGPACK_OBJECT_POSITIVE_INTEGER) {
		fprintf(stderr, "%s(%i): decode failed, illegal type(%i)\n",
				__FILE__, __LINE__, p->type);
		goto decode_failed;
	}
	msg->version = (uint16_t) p->via.u64;
	p++;

	// (4) message encoding
	if (p->type != MSGPACK_OBJECT_POSITIVE_INTEGER) {
		fprintf(stderr, "%s(%i): decode failed, illegal type(%i)\n",
				__FILE__, __LINE__, p->type);
		goto decode_failed;
	}
	msg->encoding = (dynqits_enc_t) (int) p->via.u64;
	p++;

	// (5) payload (message) type
	if (p->type != MSGPACK_OBJECT_POSITIVE_INTEGER) {
		fprintf(stderr, "%s(%i): decode failed, illegal type(%i)\n",
				__FILE__, __LINE__, p->type);
		goto decode_failed;
	}
	msg->type = (dynqits_typ_t) (int) p->via.u64;
	p++;

	// (6) payload
	if (p->type != MSGPACK_OBJECT_BIN) {
		fprintf(stderr, "%s(%i): decode failed, illegal type(%i)\n",
				__FILE__, __LINE__, p->type);
		goto decode_failed;
	}
	if (p->via.bin.size <= 0) {
		fprintf(stderr, "%s(%i): decode failed, illegal msg size(%i)\n",
				__FILE__, __LINE__, p->via.bin.size);
		goto decode_failed;
	}
	msg->buf.p = calloc(1, p->via.bin.size);
	msg->buf.size = p->via.bin.size;
	memcpy(msg->buf.p, p->via.bin.ptr, msg->buf.size);
	p++;

	msgpack_zone_destroy(&mempool);
	return msg;

  decode_failed:
	dynqits_msg_delete(msg);
	msgpack_zone_destroy(&mempool);
	return NULL;
}
