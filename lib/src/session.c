
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "MQTTClient.h"
#include "dynqits.h"
#include "dynqits_msg.h"

#define DYNQITS_DISCONN_TO 100	// ms

/**
 * Session instance
 */
typedef struct dynqits {
	/** e.g. "tcp://localhost:1883" */
	char *broker;

	/** unit number (configurable) */
	uint32_t unit;

	/** user context */
	void *user;

	/** unique unit identifier */
	uuid_t uuid;

	/** message encoding used for all publications */
	dynqits_enc_t encoding;

	/** default message quality of service used for publications */
	dynqits_qos_t qos;

	/** will be called when message has arrived at broker */
	dynqits_delivered_f delivered;

	/** will be called when new message has been received */
	dynqits_incoming_f incoming;

	/** will be called in case of errors */
	dynqits_exception_f exception;

	/** handle to mqtt session */
	MQTTClient client;

} dynqits_t;

static void mqtt_connectionLost(void *context, char *cause)
{
	dynqits_t *session = (dynqits_t *) context;
	session->exception(session->user, cause, DYNQ_EXC_CONNLOST);
}

static int
mqtt_incoming(void *context, char *topic, int len,
			  MQTTClient_message * message)
{
	int rc = 1;
	dynqits_t *session = (dynqits_t *) context;

	dynqits_msg_t *msg =
		dynqits_decode(message->payload, message->payloadlen);

	MQTTClient_freeMessage(&message);
	MQTTClient_free(topic);

	if (msg) {
		rc = session->incoming(session->user, msg->type, msg->buf.p,
							   msg->buf.size);
		dynqits_msg_delete(msg);
	}
	return rc;
}

static void mqtt_delivered(void *context, MQTTClient_deliveryToken dt)
{
	dynqits_t *session = (dynqits_t *) context;
	session->delivered(session->user, (int) dt);
}

int dynqits_create(dynqits_init_t * init, uuid_t uuid, void **handle)
{
	if (handle == NULL || init == NULL || init->broker == NULL ||
		init->delivered == NULL || init->incoming == NULL ||
		init->exception == NULL) {

		errno = EINVAL;
		return -1;
	}

	dynqits_t *session = calloc(1, sizeof(*session));
	session->broker = strdup(init->broker);
	session->delivered = init->delivered;
	session->incoming = init->incoming;
	session->exception = init->exception;
	session->unit = init->unit;
	session->qos = init->qos;
	session->encoding = init->encoding;

	if (uuid) {
		memcpy(session->uuid, uuid, sizeof(uuid_t));
	} else {
		uuid_generate(session->uuid);
	}

	int rc;
	char clientid[255];
	sprintf(clientid, "dynqits.unit.%x", session->unit);

	rc = MQTTClient_create(&(session->client), session->broker, clientid,
						   MQTTCLIENT_PERSISTENCE_NONE, NULL);
	if (rc != MQTTCLIENT_SUCCESS) {
		fprintf(stderr, "%s(%i): create client failed, mqtt error(%d)\n",
				__FILE__, __LINE__, rc);
		free(session->broker);
		free(session);
		return -1;
	}

	rc = MQTTClient_setCallbacks(session->client, session,
								 mqtt_connectionLost,
								 mqtt_incoming, mqtt_delivered);
	if (rc != MQTTCLIENT_SUCCESS) {
		fprintf(stderr, "%s(%i): set callbacks failed, mqtt error(%d)\n",
				__FILE__, __LINE__, rc);
		free(session->broker);
		free(session);
		return -1;
	}

	*handle = session;
	return 0;
}

int dynqits_destroy(void *handle)
{
	if (handle == NULL) {
		errno = EINVAL;
		return -1;
	}
	dynqits_t *session = (dynqits_t *) handle;
	dynqits_disconnect(handle);
	free(session->broker);
	free(session);
	return 0;
}

int dynqits_connect(void *handle)
{
	if (handle == NULL) {
		errno = EINVAL;
		return -1;
	}
	dynqits_t *session = (dynqits_t *) handle;

	MQTTClient_connectOptions options =
		MQTTClient_connectOptions_initializer;
	options.keepAliveInterval = 20;
	options.cleansession = 1;

	int rc = MQTTClient_connect(session->client, &options);
	if (rc != MQTTCLIENT_SUCCESS) {
		fprintf(stderr, "%s(%i): connect failed, mqtt error(%d)\n",
				__FILE__, __LINE__, rc);
	}
	return (rc == MQTTCLIENT_SUCCESS ? 0 : -1);
}

int dynqits_disconnect(void *handle)
{
	if (handle == NULL) {
		errno = EINVAL;
		return -1;
	}
	dynqits_t *session = (dynqits_t *) handle;

	int rc = MQTTClient_disconnect(session->client, DYNQITS_DISCONN_TO);
	if (rc != MQTTCLIENT_SUCCESS) {
		fprintf(stderr, "%s(%i): disconnect failed, mqtt error(%d)\n",
				__FILE__, __LINE__, rc);
	}
	return (rc == MQTTCLIENT_SUCCESS ? 0 : -1);
}

static const char *get_topic(dynqits_typ_t value)
{
	const char *key[] = {
		DYNQ_MSG_NAME_TEST,
		DYNQ_MSG_NAME_ADVERT,
		DYNQ_MSG_NAME_COMMAND,
		DYNQ_MSG_NAME_STATUS
	};
	return key[value];
}

int
dynqits_publish(void *handle, dynqits_typ_t type, const void *buf,
				size_t size, dynqits_qos_t qos, int *token)
{
	if (handle == NULL || buf == NULL || size == 0 || token == NULL) {
		errno = EINVAL;
		return -1;
	}
	if (type < DYNQ_TYP_FIRST || type > DYNQ_TYP_LAST
		|| qos < DYNQ_QOS_FIRST || qos > DYNQ_QOS_LAST) {
		errno = EINVAL;
		return -1;
	}

	dynqits_t *session = (dynqits_t *) handle;

	if (qos == DYNQ_QOS_DEFAULT)
		qos = session->qos;

	dynqits_buf_t dbuf = dynqits_encode(session->uuid, session->unit,
										session->encoding, type, buf,
										size);

	MQTTClient_message pubmsg = MQTTClient_message_initializer;
	pubmsg.payload = dbuf.p;
	pubmsg.payloadlen = dbuf.size;
	pubmsg.qos = qos;
	pubmsg.retained = 0;

	char topic[512];
	sprintf(topic, "dynqits/unit/%u/%s", session->unit, get_topic(type));

	int rc =
		MQTTClient_publishMessage(session->client, topic, &pubmsg, token);
	if (rc != MQTTCLIENT_SUCCESS) {
		fprintf(stderr, "%s(%i): publish failed, mqtt error(%d)\n",
				__FILE__, __LINE__, rc);
	}

	dynqits_buf_delete(dbuf);
	return (rc == MQTTCLIENT_SUCCESS ? 0 : -1);
}

int dynqits_subscribe(void *handle, dynqits_qos_t qos, dynqits_typ_t * type, 
					  uint32_t * unit)
{
	if (handle == NULL || qos < DYNQ_QOS_FIRST || qos > DYNQ_QOS_LAST) {
		errno = EINVAL;
		return -1;
	}

	if (type != NULL) {
		if (*type < DYNQ_TYP_FIRST || *type > DYNQ_TYP_LAST) {
			errno = EINVAL;
			return -1;
		}
	}
	
	char topic[512], u[128], t[64];
	dynqits_t *session = (dynqits_t *) handle;

	if (unit == NULL && type == NULL) {
		sprintf(topic, "dynqits/unit/#");
	}
	else {
		if (unit == NULL) sprintf(u, "+");
		else sprintf(u, "%u", *unit);

		if (type == NULL) sprintf(t, "+");
		else sprintf(t, "%s", get_topic(*type));
		
		sprintf(topic, "dynqits/unit/%s/%s", u, t);
	}
	
	int rc = MQTTClient_subscribe(session->client, topic, qos);
	if (rc != MQTTCLIENT_SUCCESS) {
		fprintf(stderr, "%s(%i): subscribe failed, mqtt error(%d)\n",
				__FILE__, __LINE__, rc);
	}

	return (rc == MQTTCLIENT_SUCCESS ? 0 : -1);
}
