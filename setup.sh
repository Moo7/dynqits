#!/bin/bash
set -e

dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
prefix=$HOME/.local

fname=$(basename "${BASH_SOURCE[0]}")
name="${fname%.*}"
usage="
NAME
	$name - setup advertisement publisher

SYNOPSIS
	./$fname [OPTION]

OPTIONS
	-p install prefix [default: $prefix]
	-h shows this help
"

while getopts :p:h opt; do
	case $opt in
		p)
			prefix=$(realpath $OPTARG)
		;;

		h)
			echo "$usage"; exit 0;
		;;

		\?)
			echo "$usage"; exit 1;
		;;
	esac
done

sudo dnf install -y \
	jansson jansson-devel jansson-devel-doc \
	msgpack msgpack-devel \
	uuid uuid-devel \
	waf indent cppcheck

mkdir -p $dir/ext
cd $dir/ext
git clone http://git.eclipse.org/gitroot/paho/org.eclipse.paho.mqtt.c.git mqtt
cd mqtt
make install prefix=$prefix

cp src/MQTTClient.h $prefix/include
cp src/MQTTClientPersistence.h $prefix/include

ln -sf $prefix/lib/libpaho-mqtt3c.so.1 $prefix/lib/libpaho-mqtt3c.so
ln -sf $prefix/lib/libpaho-mqtt3cs.so.1 $prefix/lib/libpaho-mqtt3cs.so
ln -sf $prefix/lib/libpaho-mqtt3a.so.1 $prefix/lib/libpaho-mqtt3a.so
ln -sf $prefix/lib/libpaho-mqtt3as.so.1 $prefix/lib/libpaho-mqtt3as.so


